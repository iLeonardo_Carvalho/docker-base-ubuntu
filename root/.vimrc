#
set backspace=indent,eol,start
set nocompatible
set term=ansi
imap ^[OA <ESC>ki
imap ^[OB <ESC>ji
imap ^[OC <ESC>li
imap ^[OD <ESC>hi