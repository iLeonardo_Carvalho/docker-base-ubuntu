![Ubuntu Logo](http://design.ubuntu.com/wp-content/uploads/ubuntu-logo22.png)




Docker Base/Ubuntu
===================

Este container contém é uma solução de correções e base para não repetir códigos na construção e aplicando a boa pratica de programação no Dockerfile.

> OBS.:
>
> Devido às modificações durante o [Docker CHANGELOG](https://github.com/docker/docker/blob/master/CHANGELOG.md), foi aplicado uma padronização de como construir o Dockerfile e não repetir códigos e tentar minimizar o uso do RUN (Runtime), pois na versão 0.7.2 (16/12/2013) não pode utrapassar mais de 27 image depth.

------



[TOC]

## Requisitos

Foi utilizado a versão **Release** acessado pelo site [Oficial do Ubuntu](cdimage.ubuntu.com) no dia **20 de dezembro de 2016**, não foi modificado a fonte para manter a segurança e às modificações estão no **Dockerfile**.

> **Notas:**

> - [UBUNTU](#UBUNTU): v16.04 (Xenial)

> **Obs.:**

> - Foi instalado os seguintes pacotes essenciais:
>
>
> - [vim-tiny](#vim-tiny): Editor de texto
>
> - [net-tools](#net-tools): ifconfig
>
> - [iputils-ping](#iputils-ping): ping
>
> - [procps](#procps): ps
>
> - **Recomendo a não utilização dos seguintes pacotes:**
>
> - [software-properties-common](#software-properties-common): add-apt-repository
>
> - [wget](#wget): Download
>
> - [curl](#curl): Download
>
>   Os comando **ADD** pode substituir o **wget/curl** e o **software-properties-common** além de trazer muita biblioteca para ser executado, aumenta muito o tamanho do contêiner e recomendo visitar o site [launchpad](https://launchpad.net/) que traz um meio manual de adicionar o repositório com as keys necessários do pacote que deseja instalar.

----------


Comandos Básicos
-------------------

#### Uso

Utilize:

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/base-ubuntu .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/base-ubuntu
```

Para Executar:

```
docker run -it ileonardo/base-ubuntu
```

Ou:

```
docker run -dit ileonardo/base-ubuntu
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi $(docker images -f "dangling=true" -q)
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
# Unix
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host

# Windows
docker cp <ID_Container>:/caminho/no/container/arquivo c:\caminho\no\host
```

Copiar um arquivo do **HOST** para o Container:

```
# Unix
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container

# Windows
docker cp c:\caminho\no\host\arquivo <ID_Container>:/caminho/no/container
```

Obter informações do container (PATH, STATUS, IP):

```
docker inspect <ID_Container>
```

Docker Toolbox:

```
docker-machine ls
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).
[^blitznote/debootstrap-amd64]: Comparação de serviços que podem ser removidos, disponível no [Docker Hub](https://hub.docker.com/r/blitznote/debootstrap-amd64/).
[^phusion/baseimage]: Segurança no Contêiner, disponível no [Docker Hub](https://hub.docker.com/r/phusion/baseimage/).